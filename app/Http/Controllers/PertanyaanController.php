<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        //dd($request->all());

        $request->validate([
            'judul' => 'required|unique:pertanyaans',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaans')-> insert([
            "judul" => $request["judul"],
            "isi" => $request ["isi"]
        ]);

        return redirect('/pertanyaan') -> with('success', 'Post Berhasil Disimpan!');
    }

    public function index(){
        $posts = DB::table('pertanyaans')->get(); // select * from tabel
        // dd($post);
        return view('pertanyaan.index', compact('posts'));
    }

    public function show($pertanyaan_id){
        $post = DB::table('pertanyaans')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($pertanyaan_id){
        $post = DB::table('pertanyaans')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaans',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaans')
            -> where('id', $pertanyaan_id)
            -> update([
                "judul" => $request["judul"],
                "isi" => $request ["isi"]
            ]);

        return redirect('/pertanyaan') -> with('success', 'Berhasil Update Pertanyaan!');
    }

    public function destroy($pertanyaan_id){
        $query = DB::table('pertanyaans')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan') -> with('success', 'Pertanyaan Berhasil Dihapus!');
    }
}
