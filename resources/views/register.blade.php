<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Form Sign Up</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <form action="/welcome1" method="post">
            @csrf
            <p><b>Sign Up Form</b></p>
            <div>
                <label for="fname">First name:</label>
                <br><br>
                <input type="text" name="fname" id="fname">
                <br><br>
                <label for="lname">Last name:</label>
                <br><br>
                <input type="text" name="lname" id="lname">
                <br><br>
            </div>
            <div>
                <label>Gender:</label>
                <br><br>
                <input type="radio"> Male <br>
                <input type="radio"> Female <br>
                <input type="radio"> Other
                <br><br>
            </div>
            <div>
                <label>Nationality:</label>
                <br><br>
                <select>
                    <option>Indonesian</option>
                    <option>English</option>
                </select>
                <br><br>
            </div>
            <div>
                <label>Language Spoken:</label>
                <br><br>
                <input type="checkbox" name="Language" value="Bahasa Indonesia"> Bahasa Indonesia <br>
                <input type="checkbox" name="Language" value="English"> English <br>
                <input type="checkbox" name="Language" value="Other"> Other
                <br><br>
            </div>
            <div>
                <label>Bio:</label>
                <br><br>
                <textarea cols="30" rows="10"></textarea>
                <br><br>
            </div>
            <div>
                <input type="submit" value="Sign Up">
            </div>"
        </form>
    </body>
</html>